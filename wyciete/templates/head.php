<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $title?></title>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="keywords" content="<?php echo $keywords?>"/>
        <meta name="description" content=""/>
        <link rel="icon" href="img/favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
        <link href="css/style.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700|Open+Sans:400,700,600,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'/>
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/js.js"></script>
    </head>
    <body>
        <div id="container">