<header id="main-header">
    <div class="narrow-container">
        <nav id="main-nav">
            <ul class="left-side">
                <li>
                    <a href="/o-nas.php" data-link="o-nas" class="active"><span>O nas</span></a>
                </li>
                <li>
                    <a href="blog.html" data-link="blog"><span>Blog</span></a>
                </li>
                <li>
                    <a href="/oferta.php" data-link="oferta"><span>Oferta</span></a>
                </li>
            </ul>
            <ul class="right-side">
                <li>
                    <a href="pro-bono.html" data-link="pro-bono"><span>Pro bono</span></a>
                </li>
                <li>
                    <a href="/tekstowka.php"><span>Projekt 8.2</span></a>
                </li>
                <li>
                    <a href="/kontakt.php" data-link="kontakt"><span>Kontakt</span></a>
                </li>
            </ul>
        <figure class="close-menu"></figure>
        </nav>
        <div class="logo-wrapper">
            <div class="logo">
                <a href="/" title="<?php echo $title?>"><img src="img/logo.png" alt="<?php echo $title?>"/></a>
            </div>
        </div>
        <div class="menu-button"><figure></figure></div>
    </div>
        <div class="dotted-bar"></div>
</header>