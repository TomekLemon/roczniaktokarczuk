<?php

namespace Ls\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LsUserBundle:Default:index.html.twig', array('name' => $name));
    }
}
