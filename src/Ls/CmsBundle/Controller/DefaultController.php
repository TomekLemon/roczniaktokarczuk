<?php

namespace Ls\CmsBundle\Controller;

use Ls\CmsBundle\Form\ContactType;
use Ls\CmsBundle\Utils\Tools;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $oferty = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Oferta', 'c')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $zespol = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Specjalista', 'c')
            ->where('c.na_glownej = 1')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $blog_url = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('blog_url')->getValue();;
        $json = file_get_contents($blog_url . '/wp-json/posts?filter[posts_per_page]=3&filter[order_by]=date&filter[order]=DESC');
        $posts = json_decode($json);
        $posty = array();
        foreach ($posts as $post) {
            $wiadomosc = array(
                'title' => $post->title,
                'url' => $post->link,
                'content' => html_entity_decode(Tools::truncateWord(strip_tags($post->excerpt), 210, '...')),
                'date' => new \DateTime($post->date)
            );
            $posty[] = $wiadomosc;
        }

        return $this->render('LsCmsBundle:Default:index.html.twig', array(
            'oferty' => $oferty,
            'zespol' => $zespol,
            'posty' => $posty
        ));
    }

    public function kontaktAction() {
        $form = $this->createForm(new ContactType($this->container), null, array(
            'action' => $this->container->get('router')->generate('lscms_kontakt_send'),
            'method' => 'PUT',
        ));

        return $this->render('LsCmsBundle:Default:kontakt.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function probonoAction() {
        return $this->render('LsCmsBundle:Default:probono.html.twig', array());
    }

    public function onasAction() {
        $em = $this->getDoctrine()->getManager();

        $wyroznieni = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Specjalista', 'c')
            ->where('c.wyrozniony = 1')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        $reszta = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Specjalista', 'c')
            ->where('c.wyrozniony = 0')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Default:onas.html.twig', array(
            'wyroznieni' => $wyroznieni,
            'reszta' => $reszta
        ));
    }

    public function ajaxKontaktSendAction() {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(new ContactType($this->container), null, array(
            'action' => $this->container->get('router')->generate('lscms_kontakt_send'),
            'method' => 'PUT',
        ));

        $form->handleRequest($this->get('request'));
        $response = array(
            'result' => 'ERROR',
            'form' => '',
            'errors' => array()
        );

        if ($form->isValid()) {
            $name = $form->get('name')->getData();
            $phone = $form->get('phone')->getData();
            $email = $form->get('email')->getData();
            $time = $form->get('time')->getData();
            $content = $form->get('content')->getData();

            $message_txt = '<h3>Wiadomość z formularza kontaktowego:</h3>';
            $message_txt .= 'Imię i nazwisko: ' . $name . '<br />';
            $message_txt .= 'Numer telefony: ' . $phone . '<br />';
            $message_txt .= 'Adres e-mail: <a href="mailto:' . $email . '">' . $email . '</a><br />';
            switch ($time) {
                case '8':
                    $message_txt .= 'Preferowany czas kontaktu: 8:00-10:00<br />';
                    break;

                case '10':
                    $message_txt .= 'Preferowany czas kontaktu: 10:00-12:00<br />';
                    break;

                case '12':
                    $message_txt .= 'Preferowany czas kontaktu: 12:00-14:00<br />';
                    break;

                case '14':
                    $message_txt .= 'Preferowany czas kontaktu: 14:00-16:00<br />';
                    break;

                case '18':
                    $message_txt .= 'Preferowany czas kontaktu: 18:00-20:00<br />';
                    break;
            }
            if (!empty($content)) {
                $message_txt .= '<h3>Treść wiadomości:</h3>';
                $message_txt .= nl2br($content);
            }

            $email_to = $em->getRepository('LsCmsBundle:Settings')->findOneByLabel('email_to')->getValue();

            $message = \Swift_Message::newInstance()
                ->setSubject('Wiadomość z formularza kontaktowego')
                ->setFrom(array($this->container->getParameter('mailer_user') => 'RTIP / Mailer'))
                ->setTo($email_to)
                ->setBody($message_txt, 'text/html')
                ->addPart(strip_tags($message_txt), 'text/plain');

            if (!empty($email)) {
                $message->setReplyTo($email);
            }

            $mailer = $this->get('mailer');
            $mailer->send($message);
            $spool = $mailer->getTransport()->getSpool();
            $transport = $this->container->get('swiftmailer.transport.real');
            $spool->flushQueue($transport);

            $form = $this->createForm(new ContactType($this->container), null, array(
                'action' => $this->container->get('router')->generate('lscms_kontakt_send'),
                'method' => 'PUT',
            ));
            $response['form'] = iconv("UTF-8", "UTF-8//IGNORE", $this->render('LsCmsBundle:Default:kontakt.html.twig', array(
                'form' => $form->createView()
            ))->getContent());
            $response['result'] = 'OK';
        } else {
            $response['errors'] = Tools::getFormErrors($form);
        }

        return new JsonResponse($response);
    }
}
