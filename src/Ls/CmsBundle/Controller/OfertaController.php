<?php

namespace Ls\CmsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OfertaController extends Controller {
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $oferty = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Oferta', 'c')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        return $this->render('LsCmsBundle:Oferta:index.html.twig', array(
            'entities' => $oferty,
        ));
    }

    public function showAction($slug) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->createQueryBuilder()
            ->select('c', 's', 'ss')
            ->from('LsCmsBundle:Oferta', 'c')
            ->leftJoin('c.specialists', 's')
            ->leftJoin('s.specjalista', 'ss')
            ->where('c.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Oferta entity.');
        }

        $oferty = $em->createQueryBuilder()
            ->select('c')
            ->from('LsCmsBundle:Oferta', 'c')
            ->orderBy('c.arrangement', 'ASC')
            ->getQuery()
            ->getResult();

        foreach($oferty as $key => $oferta) {
            if($oferta->getId() == $entity->getId()) {
                $oferty[$key]->setSelected(true);
            }
        }

        return $this->render('LsCmsBundle:Oferta:show.html.twig', array(
            'entity' => $entity,
            'oferty' => $oferty,
        ));
    }

}
