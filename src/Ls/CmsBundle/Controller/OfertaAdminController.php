<?php

namespace Ls\CmsBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class OfertaAdminController extends Controller {

    public function editAction($id = null) {
        // the key used to lookup the template
        $templateKey = 'edit';

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);

        if ($this->getRestMethod() == 'POST') {
            $form->bind($this->get('request'));

            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode() || $this->isPreviewApproved())) {
                if ($object->getSeoGenerate()) {
                    $object->setSeoTitle('to change');
                    $object->setSeoDescription('to change');
                    $object->setSeoKeywords('to change');
                }
                if ($object->getContentShortGenerate()) {
                    $object->setContentShort('to change');
                }
                $this->admin->update($object);
                $this->addFlash('sonata_flash_success', 'flash_edit_success');

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result' => 'ok',
                        'objectId' => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

                // redirect to edit mode
                return $this->redirectTo($object);
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash('sonata_flash_error', 'flash_edit_error');
                }
            } elseif ($this->isPreviewRequested()) {
                // enable the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'edit',
            'form' => $view,
            'object' => $object,
        ));
    }

    private function getMaxKolejnosc() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select('COUNT(c.id)')
            ->from('LsCmsBundle:Oferta', 'c')
            ->getQuery();

        $total = $query->getSingleScalarResult();
        return $total + 1;
    }

    public function movedownAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:Oferta')->findOneById($request->attributes->get('id'));

        $max = $this->getMaxKolejnosc();
        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc + 1;
        if ($new_kolejnosc < $max) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsCmsBundle:Oferta', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $entity_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:Oferta')->findOneById($entity_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function moveupAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();

        $target = $em->getRepository('LsCmsBundle:Oferta')->findOneById($request->attributes->get('id'));

        $old_kolejnosc = $target->getArrangement();
        $new_kolejnosc = $old_kolejnosc - 1;
        if ($new_kolejnosc > 0) {
            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder()
                ->select('c.id')
                ->from('LsCmsBundle:Oferta', 'c')
                ->where('c.arrangement = :arrangement')
                ->setParameter('arrangement', $new_kolejnosc)
                ->getQuery();

            $entity_id = $query->getSingleScalarResult();
            $entity = $em->getRepository('LsCmsBundle:Oferta')->findOneById($entity_id);
            $entity->setArrangement(0);
            $em->persist($entity);
            $em->flush();
            $target->setArrangement($new_kolejnosc);
            $em->persist($target);
            $em->flush();
            $entity->setArrangement($old_kolejnosc);
            $em->persist($entity);
            $em->flush();
        }

        return new RedirectResponse($this->admin->generateUrl('list'));
    }

    public function thumbAction() {
        $type = $this->get('request')->get('type');
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (null === $object->getPhotoAbsolutePath()) {
            return new RedirectResponse($this->admin->generateUrl('list'));
        } else {
            $size = $object->getThumbSize($type);
            $photo = $object->getPhotoSize();
            $thumb_ratio = $size['width'] / $size['height'];
            $photo_ratio = $photo['width'] / $photo['height'];

            $thumb_conf = array();
            $thumb_conf['photo_width'] = $photo['width'];
            $thumb_conf['photo_height'] = $photo['height'];
            if ($thumb_ratio < $photo_ratio) {
                $thumb_conf['width'] = round($photo['height'] * $thumb_ratio);
                $thumb_conf['height'] = $photo['height'];
                $thumb_conf['x'] = ceil(($photo['width'] - $thumb_conf['width']) / 2);
                $thumb_conf['y'] = 0;
            } else {
                $thumb_conf['width'] = $photo['width'];
                $thumb_conf['height'] = round($photo['width'] / $thumb_ratio);
                $thumb_conf['x'] = 0;
                $thumb_conf['y'] = ceil(($photo['height'] - $thumb_conf['height']) / 2);
            }

            $preview = array();
            $preview['width'] = 150;
            $preview['height'] = round(150 / $thumb_ratio);
            return $this->render('LsCmsBundle:Admin/Oferta:kadruj.html.twig', array(
                'object' => $object,
                'preview' => $preview,
                'thumb_conf' => $thumb_conf,
                'size' => $size,
                'aspect' => $thumb_ratio,
                'type' => $type,
            ));
        }
    }

    public function thumbSaveAction() {
        $type = $this->get('request')->get('type');
        $x = $this->get('request')->get('x');
        $y = $this->get('request')->get('y');
        $x2 = $this->get('request')->get('x2');
        $y2 = $this->get('request')->get('y2');
        $id = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        $object->Thumb($x, $y, $x2, $y2, $type);

        return new RedirectResponse($this->admin->generateObjectUrl('edit', $object));
    }

}
