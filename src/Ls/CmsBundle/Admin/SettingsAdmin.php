<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SettingsAdmin extends Admin {

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'description'
    );

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('label', null, array('label' => 'Etykieta'));
        $formMapper->add('description', null, array('label' => 'Opis', 'required' => false));
        $formMapper->add('value', null, array('label' => 'Wartość', 'required' => false));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('label', null, array('label' => 'Etykieta'));
        $datagridMapper->add('value', null, array('label' => 'Wartość'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('description', null, array('label' => 'Opis'));
        $listMapper->add('value', null, array('label' => 'Wartość', 'template' => 'LsCmsBundle:Admin\Settings:list_value.html.twig'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}
