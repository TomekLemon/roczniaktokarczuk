<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class SpecjalistaAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();
        $formMapper->with('Treść');
        $formMapper->add('name', null, array('label' => 'Imię i nazwisko', 'required' => true));
        $formMapper->add('stanowisko', null, array('label' => 'Stanowisko', 'required' => false));
        $formMapper->add('email', null, array('label' => 'Adres e-mail', 'required' => false));
        $formMapper->add('wyrozniony', null, array('label' => 'Wyróżniony'));
        $formMapper->add('na_glownej', null, array('label' => 'Na stronę główną'));
        $formMapper->with('Zdjęcie');
        $formMapper->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        $formMapper->setHelps($helps);
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('image', null, array('label' => 'Zdjęcie', 'template' => 'LsCmsBundle:Admin\Specjalista:list_photo.html.twig'));
        $listMapper->add('name', null, array('label' => 'Imię i nazwisko'));
        $listMapper->add('stanowisko', null, array('label' => 'Stanowisko'));
        $listMapper->add('email', null, array('label' => 'Adres e-mail'));
        $listMapper->add('wyrozniony', null, array('label' => 'Wyróżniony'));
        $listMapper->add('na_glownej', null, array('label' => 'Na stronę główną'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'movedown' => array(),
                'moveup' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Specjalista:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('specjalista-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->uploadPhoto();
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('specjalista-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->uploadPhoto();
        }
    }

}
