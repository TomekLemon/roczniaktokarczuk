<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class NewsAdmin extends Admin {

    protected $datagridValues = array(
        '_sort_order' => 'DESC',
        '_sort_by' => 'published_at'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $em = $this->modelManager->getEntityManager('Ls\CmsBundle\Entity\Gallery');
        $query = $em->createQueryBuilder()
            ->select('g')
            ->from('LsCmsBundle:Gallery', 'g')
            ->where('g.attachable = 1')
            ->getQuery();

        $helps = array();
        $formMapper->with('Treść');
        $formMapper->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący wydarzenie np. http://www.test.pl/wydarzenia/<b>wydarzenie</b>';
        }
        $formMapper->add('published_at', 'date', array('label' => 'Data publikacji', 'required' => false));
        $formMapper->add('content_short_generate', null, array('label' => 'Automatycznie generuj krótką treść'));
        $formMapper->add('content_short', 'textarea', array('label' => 'Krótka treść', 'required' => false, 'attr' => array('rows' => 5)));
        $formMapper->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg')));
        $formMapper->add('gallery', 'sonata_type_model', array('query' => $query, 'label' => 'Galeria', 'required' => false));
        $formMapper->add('content_under', null, array('label' => 'Treść pod galerią', 'attr' => array('class' => 'wysiwyg')));
        $formMapper->add('seo_generate', null, array('label' => 'Generuj opcje SEO'));
        $formMapper->add('seo_title', null, array('label' => 'SEO Title', 'required' => false));
        $formMapper->add('seo_keywords', 'textarea', array('label' => 'SEO Keywords', 'required' => false));
        $formMapper->add('seo_description', 'textarea', array('label' => 'SEO Description', 'required' => false, 'attr' => array('rows' => 3)));
        $formMapper->with('Zdjęcie');
        $formMapper->add('file', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('title', null, array('label' => 'Tytuł'));
        $datagridMapper->add('content', null, array('label' => 'Treść'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('title', null, array('label' => 'Tytuł'));
        $listMapper->add('published_at', 'date', array('label' => 'Data publikacji'));
        $listMapper->add('gallery', null, array('label' => 'Galeria', 'template' => 'LsCmsBundle:Admin\News:list_gallery.html.twig'));
        $listMapper->add('content_short', null, array('label' => 'Krótka treść', 'class' => 'test'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\News:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('news-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFile()) {
            $sFileName = uniqid('news-image-') . '.' . $entity->getFile()->guessExtension();
            $entity->setPhoto($sFileName);
            $this->saveFile($entity);
        }
    }

    public function saveFile($entity) {
        $entity->upload();
    }

}
