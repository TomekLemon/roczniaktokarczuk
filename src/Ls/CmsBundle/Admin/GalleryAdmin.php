<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class GalleryAdmin extends Admin {

    protected $datagridValues = array(
        '_sort_order' => 'ASC',
        '_sort_by' => 'title'
    );

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('photos', $this->getRouterIdParameter() . '/photos');
        if ($this->hasParentFieldDescription()) {
            $collection->remove('create');
        }
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();
        $formMapper->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący galerię np. http://www.test.pl/galeria/<b>galeria</b>';
        }
        $formMapper->add('on_list', null, array('label' => 'Wyświetlaj na liście galerii', 'required' => false));
        $formMapper->add('attachable', null, array('label' => 'Dołączalna do podstron i aktualności', 'required' => false));
        $formMapper->add('seo_generate', null, array('label' => 'Generuj SEO Title'));
        $formMapper->add('seo_title', null, array('label' => 'SEO Title', 'required' => false));
        $formMapper->add('seo_keywords', 'textarea', array('label' => 'SEO Keywords', 'required' => false));
        $formMapper->add('seo_description', 'textarea', array('label' => 'SEO Description', 'required' => false, 'attr' => array('rows' => 3)));
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('title', null, array('label' => 'Nazwa'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->addIdentifier('title', null, array('label' => 'Nazwa'));
        $listMapper->add('on_list', null, array('label' => 'Widoczna na liście'));
        $listMapper->add('attachable', null, array('label' => 'Dołączalna'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'photos' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}
