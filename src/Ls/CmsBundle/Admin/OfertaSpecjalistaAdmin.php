<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OfertaSpecjalistaAdmin extends Admin {

    protected function configureFormFields(FormMapper $formMapper) {
        $formMapper->add('specjalista', 'sonata_type_model_list', array('label' => 'Specjalista'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('specjalista', null, array('label' => 'Specjalista'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }
}
