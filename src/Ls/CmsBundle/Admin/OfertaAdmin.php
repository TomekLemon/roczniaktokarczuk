<?php

namespace Ls\CmsBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class OfertaAdmin extends Admin {

    public function createQuery($context = 'list') {
        $query = parent::createQuery($context);

        $query->orderBy('o.arrangement', 'ASC');

        return $query;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('moveup', $this->getRouterIdParameter() . '/moveup');
        $collection->add('movedown', $this->getRouterIdParameter() . '/movedown');
        $collection->add('thumb', $this->getRouterIdParameter() . '/thumb/{type}');
        $collection->add('thumbSave', $this->getRouterIdParameter() . '/thumbSave');
    }

    protected function configureFormFields(FormMapper $formMapper) {
        $helps = array();
        $formMapper->with('Treść');
        $formMapper->add('title', null, array('label' => 'Tytuł', 'required' => true));
        if ($this->getRoot()->getSubject()->getId() > 0) {
            $formMapper->add('slug', null, array('label' => 'Końcówka adresu URL', 'required' => false));
            $helps['slug'] = 'Element linku URL identyfikujący ofertę np. http://www.test.pl/oferty/<b>oferta</b>';
        }
        $formMapper->add('content_short_generate', null, array('label' => 'Automatycznie generuj krótką treść'));
        $formMapper->add('content_short', 'textarea', array('label' => 'Krótka treść', 'required' => false, 'attr' => array('rows' => 5)));
        $formMapper->add('content', null, array('label' => 'Treść', 'attr' => array('class' => 'wysiwyg')));
        $formMapper->add('seo_generate', null, array('label' => 'Generuj opcje SEO'));
        $formMapper->add('seo_title', null, array('label' => 'SEO Title', 'required' => false));
        $formMapper->add('seo_keywords', 'textarea', array('label' => 'SEO Keywords', 'required' => false));
        $formMapper->add('seo_description', 'textarea', array('label' => 'SEO Description', 'required' => false, 'attr' => array('rows' => 3)));
        $formMapper->with('Zdjęcie');
        $formMapper->add('file_icon', 'file', array('label' => 'Nowa ikona', 'required' => false));
        $formMapper->add('file_icon_details', 'file', array('label' => 'Nowa ikona dla szczegółów', 'required' => false));
        $formMapper->add('file_photo', 'file', array('label' => 'Nowe zdjęcie', 'required' => false));
        $formMapper->with('Specjaliści');
        $formMapper->add('specialists', 'sonata_type_collection',
            array(
                'cascade_validation' => true,
                'by_reference' => false,
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
                'admin_code' => 'ls.cms.admin.ofertyspecjalisci'
            )
        );
        $formMapper->setHelps($helps);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper) {
        $datagridMapper->add('title', null, array('label' => 'Tytuł'));
        $datagridMapper->add('content', null, array('label' => 'Treść'));
    }

    protected function configureListFields(ListMapper $listMapper) {
        $listMapper->add('icon', null, array('label' => 'Ikona', 'template' => 'LsCmsBundle:Admin\Oferta:list_icon.html.twig'));
        $listMapper->add('icon_details', null, array('label' => 'Ikona szczegółów', 'template' => 'LsCmsBundle:Admin\Oferta:list_icon_details.html.twig'));
        $listMapper->add('title', null, array('label' => 'Tytuł'));
        $listMapper->add('content_short', null, array('label' => 'Krótka treść'));
        $listMapper->add('_action', 'actions', array(
            'label' => 'Opcje',
            'actions' => array(
                'movedown' => array(),
                'moveup' => array(),
                'edit' => array(),
                'delete' => array(),
            )
        ));
    }

    public function getTemplate($name) {
        switch ($name) {
            case 'edit':
                return 'LsCmsBundle:Admin\Oferta:edit.html.twig';
                break;

            default:
                return parent::getTemplate($name);
                break;
        }
    }

    public function prePersist($entity) {
        if (null !== $entity->getFilePhoto()) {
            $sFileName = uniqid('oferta-image-') . '.' . $entity->getFilePhoto()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->uploadPhoto();
        }
        if (null !== $entity->getFileIcon()) {
            $sFileName = uniqid('oferta-icon-') . '.' . $entity->getFileIcon()->guessExtension();
            $entity->setIcon($sFileName);
            $entity->uploadIcon();
        }
        if (null !== $entity->getFileIconDetails()) {
            $sFileName = uniqid('oferta-icon-details-') . '.' . $entity->getFileIconDetails()->guessExtension();
            $entity->setIconDetails($sFileName);
            $entity->uploadIconDetails();
        }
    }

    public function preUpdate($entity) {
        if (null !== $entity->getFilePhoto()) {
            $sFileName = uniqid('oferta-image-') . '.' . $entity->getFilePhoto()->guessExtension();
            $entity->setPhoto($sFileName);
            $entity->uploadPhoto();
        }
        if (null !== $entity->getFileIcon()) {
            $sFileName = uniqid('oferta-icon-') . '.' . $entity->getFileIcon()->guessExtension();
            $entity->setIcon($sFileName);
            $entity->uploadIcon();
        }
        if (null !== $entity->getFileIconDetails()) {
            $sFileName = uniqid('oferta-icon-details-') . '.' . $entity->getFileIconDetails()->guessExtension();
            $entity->setIconDetails($sFileName);
            $entity->uploadIconDetails();
        }
    }

}
