<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ls\CmsBundle\Utils\Tools;
use PhpThumb\ThumbFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Specjalista
 * @ORM\Table(name="specjalista")
 * @ORM\Entity
 */
class Specjalista {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $stanowisko;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $wyrozniony;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $na_glownej;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="OfertaSpecjalista",
     *   mappedBy="specjalista",
     *   cascade={"all"},
     *   orphanRemoval=true
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $offers;

    protected $file;

    protected $biggestWidth   = 350;
    protected $biggestHeight  = 368;
    protected $bigWidth       = 287;
    protected $bigHeight      = 302;
    protected $mediumWidth    = 255;
    protected $mediumHeight   = 268;
    protected $smallWidth     = 224;
    protected $smallHeight    = 235;
    protected $smallerWidth   = 208;
    protected $smallerHeight  = 218;
    protected $smallestWidth  = 160;
    protected $smallestHeight = 168;

    /**
     * Constructor
     */
    public function __construct() {
        $this->wyrozniony = false;
        $this->na_glownej = false;
        $this->offers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Specjalista
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set stanowisko
     *
     * @param string $stanowisko
     * @return Specjalista
     */
    public function setStanowisko($stanowisko) {
        $this->stanowisko = $stanowisko;

        return $this;
    }

    /**
     * Get stanowisko
     *
     * @return string
     */
    public function getStanowisko() {
        return $this->stanowisko;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Specjalista
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Specjalista
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set wyrozniony
     *
     * @param boolean $wyrozniony
     * @return Specjalista
     */
    public function setWyrozniony($wyrozniony) {
        $this->wyrozniony = $wyrozniony;

        return $this;
    }

    /**
     * Get wyrozniony
     *
     * @return boolean
     */
    public function getWyrozniony() {
        return $this->wyrozniony;
    }

    /**
     * Set na_glownej
     *
     * @param boolean $na_glownej
     * @return Specjalista
     */
    public function setNaGlownej($na_glownej) {
        $this->na_glownej = $na_glownej;

        return $this;
    }

    /**
     * Get na_glownej
     *
     * @return boolean
     */
    public function getNaGlownej() {
        return $this->na_glownej;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Specjalista
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Specjalista
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Specjalista
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Add oferta
     *
     * @param OfertaSpecjalista $oferta
     * @return Specjalista
     */
    public function addOffer(OfertaSpecjalista $oferta) {
        $oferta->setSpecjalista($this);
        $this->offers[] = $oferta;

        return $this;
    }

    /**
     * Remove oferta
     *
     * @param OfertaSpecjalista $oferta
     */
    public function removeOffer(OfertaSpecjalista $oferta) {
        $this->offers->removeElement($oferta);
    }

    /**
     * Get offers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOffers() {
        return $this->offers;
    }

    public function __toString() {
        if (is_null($this->getName())) {
            return 'NULL';
        }
        return $this->getName();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'biggest':
                $size['width'] = $this->biggestWidth;
                $size['height'] = $this->biggestHeight;
                break;
            case 'big':
                $size['width'] = $this->bigWidth;
                $size['height'] = $this->bigHeight;
                break;
            case 'medium':
                $size['width'] = $this->mediumWidth;
                $size['height'] = $this->mediumHeight;
                break;
            case 'small':
                $size['width'] = $this->smallWidth;
                $size['height'] = $this->smallHeight;
                break;
            case 'smaller':
                $size['width'] = $this->smallerWidth;
                $size['height'] = $this->smallerHeight;
                break;
            case 'smallest':
                $size['width'] = $this->smallestWidth;
                $size['height'] = $this->smallestHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'biggest':
                    $sThumbName = Tools::thumbName($this->photo, '_bt');
                    break;
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
                case 'smaller':
                    $sThumbName = Tools::thumbName($this->photo, '_sr');
                    break;
                case 'smallest':
                    $sThumbName = Tools::thumbName($this->photo, '_st');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'biggest':
                    $sThumbName = Tools::thumbName($this->photo, '_bt');
                    break;
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
                case 'small':
                    $sThumbName = Tools::thumbName($this->photo, '_s');
                    break;
                case 'smaller':
                    $sThumbName = Tools::thumbName($this->photo, '_sr');
                    break;
                case 'smallest':
                    $sThumbName = Tools::thumbName($this->photo, '_st');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFile(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFile() {
        return $this->file;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_bt = Tools::thumbName($filename, '_bt');
            $filename_b = Tools::thumbName($filename, '_b');
            $filename_m = Tools::thumbName($filename, '_m');
            $filename_s = Tools::thumbName($filename, '_s');
            $filename_sr = Tools::thumbName($filename, '_sr');
            $filename_st = Tools::thumbName($filename, '_st');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_bt)) {
                @unlink($filename_bt);
            }
            if (file_exists($filename_b)) {
                @unlink($filename_b);
            }
            if (file_exists($filename_m)) {
                @unlink($filename_m);
            }
            if (file_exists($filename_s)) {
                @unlink($filename_s);
            }
            if (file_exists($filename_sr)) {
                @unlink($filename_sr);
            }
            if (file_exists($filename_st)) {
                @unlink($filename_st);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/specjalisci';
    }

    public function uploadPhoto() {
        if (null === $this->file) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameBT = Tools::thumbName($sSourceName, '_bt');
        $aThumbSizeBT = $this->getThumbSize('biggest');
        $thumb->adaptiveResize($aThumbSizeBT['width'] + 2, $aThumbSizeBT['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeBT['width'], $aThumbSizeBT['height']);
        $thumb->save($sThumbNameBT);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameB = Tools::thumbName($sSourceName, '_b');
        $aThumbSizeB = $this->getThumbSize('big');
        $thumb->adaptiveResize($aThumbSizeB['width'] + 2, $aThumbSizeB['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeB['width'], $aThumbSizeB['height']);
        $thumb->save($sThumbNameB);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_m');
        $aThumbSizeM = $this->getThumbSize('medium');
        $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameS = Tools::thumbName($sSourceName, '_s');
        $aThumbSizeS = $this->getThumbSize('small');
        $thumb->adaptiveResize($aThumbSizeS['width'] + 2, $aThumbSizeS['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeS['width'], $aThumbSizeS['height']);
        $thumb->save($sThumbNameS);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameSR = Tools::thumbName($sSourceName, '_sr');
        $aThumbSizeSR = $this->getThumbSize('smaller');
        $thumb->adaptiveResize($aThumbSizeSR['width'] + 2, $aThumbSizeSR['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeSR['width'], $aThumbSizeSR['height']);
        $thumb->save($sThumbNameSR);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameST = Tools::thumbName($sSourceName, '_st');
        $aThumbSizeST = $this->getThumbSize('smallest');
        $thumb->adaptiveResize($aThumbSizeST['width'] + 2, $aThumbSizeST['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeST['width'], $aThumbSizeST['height']);
        $thumb->save($sThumbNameST);

        unset($this->file_photo);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

}