<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Ls\CmsBundle\Utils\Tools;
use PhpThumb\ThumbFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Oferta
 * @ORM\Table(name="oferta")
 * @ORM\Entity
 */
class Oferta {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $title;

    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $old_slug;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $content_short_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $content_short;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $icon_details;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $photo;

    /**
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $arrangement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $seo_generate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $seo_description;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(
     *   targetEntity="OfertaSpecjalista",
     *   mappedBy="oferta",
     *   cascade={"all"},
     *   orphanRemoval=true
     * )
     * @var \Doctrine\Common\Collections\Collection
     */
    private $specialists;

    private $selected;

    protected $file_icon;
    protected $file_icon_details;
    protected $file_photo;

    protected $bigWidth     = 349;
    protected $bigHeight    = 455;
    protected $mediumWidth  = 285;
    protected $mediumHeight = 372;

    /**
     * Constructor
     */
    public function __construct() {
        $this->content_short_generate = true;
        $this->seo_generate = true;
        $this->selected = false;
        $this->specialists = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Oferta
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Oferta
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set old_slug
     *
     * @param string $old_slug
     * @return Oferta
     */
    public function setOldSlug($old_slug) {
        $this->old_slug = $old_slug;

        return $this;
    }

    /**
     * Get old_slug
     *
     * @return string
     */
    public function getOldSlug() {
        return $this->old_slug;
    }

    /**
     * Set content_short_generate
     *
     * @param boolean $contentShortGenerate
     * @return Oferta
     */
    public function setContentShortGenerate($contentShortGenerate) {
        $this->content_short_generate = $contentShortGenerate;

        return $this;
    }

    /**
     * Get content_short_generate
     *
     * @return boolean
     */
    public function getContentShortGenerate() {
        return $this->content_short_generate;
    }

    /**
     * Set content_short
     *
     * @param string $contentShort
     * @return Oferta
     */
    public function setContentShort($contentShort) {
        $this->content_short = $contentShort;

        return $this;
    }

    /**
     * Get content_short
     *
     * @return string
     */
    public function getContentShort() {
        return $this->content_short;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Oferta
     */
    public function setContent($content) {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent() {
        return $this->content;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return Oferta
     */
    public function setIcon($icon) {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon() {
        return $this->icon;
    }

    /**
     * Set icon_details
     *
     * @param string $icon_details
     * @return Oferta
     */
    public function setIconDetails($icon_details) {
        $this->icon_details = $icon_details;

        return $this;
    }

    /**
     * Get icon_details
     *
     * @return string
     */
    public function getIconDetails() {
        return $this->icon_details;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return Oferta
     */
    public function setPhoto($photo) {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto() {
        return $this->photo;
    }

    /**
     * Set arrangement
     *
     * @param integer $arrangement
     * @return Oferta
     */
    public function setArrangement($arrangement) {
        $this->arrangement = $arrangement;

        return $this;
    }

    /**
     * Get arrangement
     *
     * @return integer
     */
    public function getArrangement() {
        return $this->arrangement;
    }

    /**
     * Set seo_generate
     *
     * @param boolean $seoGenerate
     * @return Oferta
     */
    public function setSeoGenerate($seoGenerate) {
        $this->seo_generate = $seoGenerate;

        return $this;
    }

    /**
     * Get seo_generate
     *
     * @return boolean
     */
    public function getSeoGenerate() {
        return $this->seo_generate;
    }

    /**
     * Set seo_title
     *
     * @param string $seoTitle
     * @return Oferta
     */
    public function setSeoTitle($seoTitle) {
        $this->seo_title = $seoTitle;

        return $this;
    }

    /**
     * Get seo_title
     *
     * @return string
     */
    public function getSeoTitle() {
        return $this->seo_title;
    }

    /**
     * Set seo_keywords
     *
     * @param string $seoKeywords
     * @return Oferta
     */
    public function setSeoKeywords($seoKeywords) {
        $this->seo_keywords = $seoKeywords;

        return $this;
    }

    /**
     * Get seo_keywords
     *
     * @return string
     */
    public function getSeoKeywords() {
        return $this->seo_keywords;
    }

    /**
     * Set seo_description
     *
     * @param string $seoDescription
     * @return Oferta
     */
    public function setSeoDescription($seoDescription) {
        $this->seo_description = $seoDescription;

        return $this;
    }

    /**
     * Get seo_description
     *
     * @return string
     */
    public function getSeoDescription() {
        return $this->seo_description;
    }

    /**
     * Set created_at
     *
     * @param \DateTime $createdAt
     * @return Oferta
     */
    public function setCreatedAt($createdAt) {
        $this->created_at = $createdAt;

        return $this;
    }

    /**
     * Get created_at
     *
     * @return \DateTime
     */
    public function getCreatedAt() {
        return $this->created_at;
    }

    /**
     * Set updated_at
     *
     * @param \DateTime $updatedAt
     * @return Oferta
     */
    public function setUpdatedAt($updatedAt) {
        $this->updated_at = $updatedAt;

        return $this;
    }

    /**
     * Get updated_at
     *
     * @return \DateTime
     */
    public function getUpdatedAt() {
        return $this->updated_at;
    }

    /**
     * Add specjalista
     *
     * @param OfertaSpecjalista $specjalista
     * @return Oferta
     */
    public function addSpecialist(OfertaSpecjalista $specjalista) {
        $specjalista->setOferta($this);
        $this->specialists[] = $specjalista;

        return $this;
    }

    /**
     * Remove specjalista
     *
     * @param OfertaSpecjalista $specjalista
     */
    public function removeSpecialist(OfertaSpecjalista $specjalista) {
        $this->specialists->removeElement($specjalista);
    }

    /**
     * Get specialists
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialists() {
        return $this->specialists;
    }

    public function setSelected($selected) {
        $this->selected = $selected;

        return $this;
    }

    public function getSelected() {
        return $this->selected;
    }

    public function __toString() {
        if (is_null($this->getTitle())) {
            return 'NULL';
        }
        return $this->getTitle();
    }

    public function getThumbSize($type) {
        $size = array();
        switch ($type) {
            case 'big':
                $size['width'] = $this->bigWidth;
                $size['height'] = $this->bigHeight;
                break;
            case 'medium':
                $size['width'] = $this->mediumWidth;
                $size['height'] = $this->mediumHeight;
                break;
        }
        return $size;
    }

    public function getThumbWebPath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
            }
            return '/' . $this->getUploadDir() . '/' . $sThumbName;
        }
    }

    public function getThumbAbsolutePath($type) {
        if (empty($this->photo)) {
            return null;
        } else {
            $sThumbName = '';
            switch ($type) {
                case 'big':
                    $sThumbName = Tools::thumbName($this->photo, '_b');
                    break;
                case 'medium':
                    $sThumbName = Tools::thumbName($this->photo, '_m');
                    break;
            }
            return $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sThumbName;
        }
    }

    public function getPhotoSize() {
        $temp = getimagesize($this->getPhotoAbsolutePath());
        $size = array(
            'width' => $temp[0],
            'height' => $temp[1]
        );
        return $size;
    }

    public function setFilePhoto(UploadedFile $file = null) {
        $this->deletePhoto();
        $this->file_photo = $file;
        if (empty($this->photo)) {
            $this->setPhoto('empty');
        } else {
            $this->setPhoto('');
        }
    }

    public function getFilePhoto() {
        return $this->file_photo;
    }

    public function deletePhoto() {
        if (!empty($this->photo)) {
            $filename = $this->getPhotoAbsolutePath();
            $filename_b = Tools::thumbName($filename, '_b');
            $filename_m = Tools::thumbName($filename, '_m');
            if (file_exists($filename)) {
                @unlink($filename);
            }
            if (file_exists($filename_b)) {
                @unlink($filename_b);
            }
            if (file_exists($filename_m)) {
                @unlink($filename_m);
            }
        }
    }

    public function getPhotoAbsolutePath() {
        return empty($this->photo) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->photo;
    }

    public function getPhotoWebPath() {
        return empty($this->photo) ? null : '/' . $this->getUploadDir() . '/' . $this->photo;
    }

    protected function getUploadRootDir() {
        // the absolute directory path where uploaded documents should be saved
        return __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'web' . DIRECTORY_SEPARATOR . $this->getUploadDir();
    }

    protected function getUploadDir() {
        // get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
        return 'upload/oferty';
    }

    public function uploadPhoto() {
        if (null === $this->file_photo) {
            return;
        }

        $sFileName = $this->getPhoto();

        $this->file_photo->move($this->getUploadRootDir(), $sFileName);

        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $sFileName;
        $thumb = ThumbFactory::create($sSourceName);
        $dimensions = $thumb->getCurrentDimensions();

        //zmniejszenie zdjecia oryginalnego jesli jest za duze
        if ($dimensions['width'] > 1024 || $dimensions['height'] > 768) {
            $thumb->resize(1026, 770);
            $thumb->crop(0, 0, 1024, 768);
            $thumb->save($sSourceName);
        }

        //tworzy wszystkie miniatury wycinajac ze srodka zdjecia
        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameB = Tools::thumbName($sSourceName, '_b');
        $aThumbSizeB = $this->getThumbSize('big');
        $thumb->adaptiveResize($aThumbSizeB['width'] + 2, $aThumbSizeB['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeB['width'], $aThumbSizeB['height']);
        $thumb->save($sThumbNameB);

        $thumb = ThumbFactory::create($sSourceName);
        $sThumbNameM = Tools::thumbName($sSourceName, '_m');
        $aThumbSizeM = $this->getThumbSize('medium');
        $thumb->adaptiveResize($aThumbSizeM['width'] + 2, $aThumbSizeM['height'] + 2);
        $thumb->crop(0, 0, $aThumbSizeM['width'], $aThumbSizeM['height']);
        $thumb->save($sThumbNameM);

        unset($this->file_photo);
    }

    public function Thumb($x, $y, $x2, $y2, $type) {
        $sSourceName = $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->getPhoto();
        $sThumbName = $this->getThumbAbsolutePath($type);
        $aThumbSize = $this->getThumbSize($type);
        $thumb = ThumbFactory::create($sSourceName);

        $cropWidth = $x2 - $x;
        $cropHeight = $y2 - $y;

        $thumb->crop($x, $y, $cropWidth, $cropHeight);
        $thumb->resize($aThumbSize['width'] + 2, $aThumbSize['height'] + 2);
        $thumb->crop(0, 0, $aThumbSize['width'], $aThumbSize['height']);
        $thumb->save($sThumbName);
    }

    public function setFileIcon(UploadedFile $file = null) {
        $this->deleteIcon();
        $this->file_icon = $file;
        if (empty($this->icon)) {
            $this->setIcon('empty');
        } else {
            $this->setIcon('');
        }
    }

    public function getFileIcon() {
        return $this->file_icon;
    }

    public function deleteIcon() {
        if (!empty($this->icon)) {
            $filename = $this->getIconAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getIconAbsolutePath() {
        return empty($this->icon) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon;
    }

    public function getIconWebPath() {
        return empty($this->icon) ? null : '/' . $this->getUploadDir() . '/' . $this->icon;
    }

    public function uploadIcon() {
        if (null === $this->file_icon) {
            return;
        }

        $sFileName = $this->getIcon();

        $this->file_icon->move($this->getUploadRootDir(), $sFileName);

        unset($this->file_icon);
    }

    public function setFileIconDetails(UploadedFile $file = null) {
        $this->deleteIconDetails();
        $this->file_icon_details = $file;
        if (empty($this->icon_details)) {
            $this->setIconDetails('empty');
        } else {
            $this->setIconDetails('');
        }
    }

    public function getFileIconDetails() {
        return $this->file_icon_details;
    }

    public function deleteIconDetails() {
        if (!empty($this->icon_details)) {
            $filename = $this->getIconDetailsAbsolutePath();
            if (file_exists($filename)) {
                @unlink($filename);
            }
        }
    }

    public function getIconDetailsAbsolutePath() {
        return empty($this->icon_details) ? null : $this->getUploadRootDir() . DIRECTORY_SEPARATOR . $this->icon_details;
    }

    public function getIconDetailsWebPath() {
        return empty($this->icon_details) ? null : '/' . $this->getUploadDir() . '/' . $this->icon_details;
    }

    public function uploadIconDetails() {
        if (null === $this->file_icon_details) {
            return;
        }

        $sFileName = $this->getIconDetails();

        $this->file_icon_details->move($this->getUploadRootDir(), $sFileName);

        unset($this->file_icon_details);
    }

}