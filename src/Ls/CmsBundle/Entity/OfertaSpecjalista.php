<?php

namespace Ls\CmsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OfertaSpecjalista
 * @ORM\Table(name="oferta_specjalista")
 * @ORM\Entity
 */
class OfertaSpecjalista {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var integer
     */
    private $id;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Oferta",
     *     inversedBy="specialists"
     * )
     * @ORM\JoinColumn(
     *     name="oferta_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var Oferta
     */
    private $oferta;

    /**
     * @ORM\ManyToOne(
     *     targetEntity="Specjalista",
     *     inversedBy="offers"
     * )
     * @ORM\JoinColumn(
     *     name="specjalista_id",
     *     referencedColumnName="id",
     *     onDelete="CASCADE"
     * )
     * @var Specjalista
     */
    private $specjalista;


    /**
     * Constructor
     */
    public function __construct() {
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set oferta
     *
     * @param Oferta $oferta
     * @return OfertaSpecjalista
     */
    public function setOferta(Oferta $oferta = null) {
        $this->oferta = $oferta;

        return $this;
    }

    /**
     * Get oferta
     *
     * @return Oferta
     */
    public function getOferta() {
        return $this->oferta;
    }

    /**
     * Set specjalista
     *
     * @param Specjalista $specjalista
     * @return OfertaSpecjalista
     */
    public function setSpecjalista(Specjalista $specjalista = null) {
        $this->specjalista = $specjalista;

        return $this;
    }

    /**
     * Get specjalista
     *
     * @return Specjalista
     */
    public function getSpecjalista() {
        return $this->specjalista;
    }

    public function __toString() {
        if (is_null($this->getSpecjalista()->getName())) {
            return 'NULL';
        }
        return $this->getSpecjalista()->getName();
    }

}