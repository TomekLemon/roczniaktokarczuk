function setMenuHeight() {
    if ($('#main-header .menu-button').is(':visible')) {
        var mn = $('#main-nav');
        var h = parseInt(mn.find('ul.left-side').outerHeight() + mn.find('ul.right-side').outerHeight());
        if (h > window.innerHeight) {
            mn.css('height', window.innerHeight);

        }
    }
}
function setMovieHeight() {
    var mc = $('#movie-container');
    if (mc.length) {
        if (mc.is(':visible')) {
            var h = window.innerHeight - 10 - $('#main-header').height();
            mc.css('height', h + 'px');
        } else {
            var video = document.getElementById('movie-container').getElementsByTagName('video')[0];
            video.pause();
        }
    }
}

function pauseIfPlaying() {
    if (document.getElementById('movie-container')) {
        var mc = $('#movie-container');
        if (mc.is(':visible')) {
            var video = document.getElementById('movie-container').getElementsByTagName('video')[0];
            if (video) {
                video.pause();
                mc.removeClass('playing');
            }
        }
    }
}
var enableAutoSelect = true;
function autoSelectMenuItem() {
    if ($('.main-page').length && enableAutoSelect) {
        var a = $('#main-nav a');
        a.each(function () {
            var t = $(this);
            var link = t.data('link');

            if (link) {
                if ($('#' + link).length) {
                    if (window.scrollY + (window.innerHeight / 2) <= $('#' + $(a[0]).data('link')).offset().top) {
                        $('#main-nav a.active').removeClass('active');
                    } else if ((window.scrollY + (window.innerHeight / 2)) >= $('#' + link).offset().top && $('#' + link).offset().top + $('#' + link).outerHeight() > window.scrollY) {
                        $('#main-nav a.active').removeClass('active');
                        t.addClass('active');
                        return;
                    }
                }
            }
        });
    }
}

$(document).ready(function () {
    if (document.getElementById('movie-container')) {
        var video = document.getElementById('movie-container').getElementsByTagName('video')[0];
        var mc = $('#movie-container');
        mc.find('video').prop('muted', true);
        $('#mute-control').addClass('muted');
        if (mc.is(':visible')) {
            mc.find('video').attr('autoplay', true);
        }
        if (video) {
            $('#play-control').on('click', function () {
                if (video.paused) {
                    video.play();
                    mc.addClass('playing');
                } else {
                    video.pause();
                    mc.removeClass('playing');
                }
            });
            $('#mute-control').on('click', function () {
                mc.find('video').prop('muted', !mc.find('video').prop('muted'));
                mc.find('video').prop('muted') ? $('#mute-control').addClass('muted') : $('#mute-control').removeClass('muted');
            });
        }
        setMovieHeight();
    }
    $(document).on('scroll', function () {
        if (window.scrollY > 150) {
            $('#container').addClass('glued');
        } else {
            $('#container').removeClass('glued');
        }
        autoSelectMenuItem();
    });

    $('#main-nav,#menu-bottom,.movie-content').on('click', 'a', function (e) {
        var t = $(this);
        var link = t.data('link');
        if (link) {
            if ($('#' + link).length) {
                e.preventDefault();
                enableAutoSelect = false;
                pauseIfPlaying();
                $('#main-nav a.active').removeClass('active');
                $('#main-nav a[data-link=' + link + ']').addClass('active');
                $('html, body').animate({
                    scrollTop: $('#' + link).offset().top
                }, 1000, function () {
                    window.location.hash = '#' + link;
                    enableAutoSelect = true;
                });
            }
        }
    });

    $('#main-header .logo').on('click', 'a', function (e) {
        if ($('.main-page').length) {
            e.preventDefault();
            $('#main-nav a.active').removeClass('active');
            $('html, body').animate({
                scrollTop: 0
            }, 1000, function () {
                window.location.hash = ''
            });
        }
    });

    $('.offers-toggle').on('click', function () {
        $('.offers-list').toggle();
    });
    var mh = $('#main-header');
    mh.on('click', '.menu-button', function () {
        $('#main-nav').addClass('opened');
    });
    mh.on('click', '.close-menu', function () {
        $('#main-nav').removeClass('opened');
    });
    setMenuHeight();
    $(window).on('resize', setMenuHeight);
    $(window).on('resize', setMovieHeight);

});

function submitContact() {
    $('div[id^="qtip-"]').each(function () {
        _qtip2 = $(this).data("qtip");
        if (_qtip2 != undefined) {
            _qtip2.destroy(true);
        }
    });

    var form = $('#form-contact');
    var data = form.serialize();
    var url = form.attr('action');
    $.ajax({
        type: "post",
        url: url,
        data: data,
        success: function (response) {
            console.log(response);
            if (response.result == 'OK') {
                $('#contact-container').html(response.form);
                initializePseudoSelect();
                alert('Twoja wiadomość została wysłana.')
            } else {
                for (var error in response.errors) {
                    $('.form-contact-' + error).qtip({
                        content: {
                            title: null,
                            text: response.errors[error]
                        },
                        position: {
                            my: 'bottom right',
                            at: 'top right',
                            adjust: {
                                x: 2,
                                y: -5
                            }
                        },
                        show: {
                            ready: true,
                            event: false
                        },
                        hide: {
                            event: 'click focus unfocus'
                        },
                        style: {
                            classes: 'qtip-red'
                        }
                    });
                }
            }
        }
    });

    return false;
}
