var pseudoselect = {
    clicks: 0,
    closeSelectOnClicksNumber: 0,
    closeClick: false
};

function initializePseudoSelect() {
    $('.pseudo-select-input').val('');
    $('.pseudo-select').each(function () {
        var not_selected = true;
        var item;
        $(this).children('.pseudo-select-list').children('.pseudo-select-list-item').each(function () {
            if ($(this).hasClass('pseudo-select-list-item-selected')) {
                not_selected = false;
            }
        });
        if (not_selected) {
            item = $(this).children('.pseudo-select-list').find(':nth-child(1)');
            $(this).children('.pseudo-select-selected').html(item.html());
            $(this).children('.pseudo-select-input').val(item.attr('name')).trigger('change');
            $(this).children('.pseudo-select-list').children('.pseudo-select-list-item').removeClass('pseudo-select-list-item-selected');
            item.addClass('pseudo-select-list-item-selected');
            pseudoselect.closeClick = false;
            pseudoselect.closeSelectOnClicksNumber = 0;
        } else {
            item = $(this).children('.pseudo-select-list').children('.pseudo-select-list-item-selected');
            $(this).children('.pseudo-select-selected').html(item.html());
            $(this).children('.pseudo-select-input').val(item.attr('name')).trigger('change');
            pseudoselect.closeClick = false;
            pseudoselect.closeSelectOnClicksNumber = 0;
        }
    });
}

$(document).on('click', 'body', function () {
    pseudoselect.clicks += 1;
    if (pseudoselect.closeClick && pseudoselect.clicks === pseudoselect.closeSelectOnClicksNumber) {
        $('.pseudo-select').removeClass('pseudo-select-clicked');
        $('.pseudo-select-list').hide();
        pseudoselect.closeClick = false;
        pseudoselect.closeSelectOnClicksNumber = 0;
    }
});

$(document).on('click', '.pseudo-select-selected', function () {
    var list = $(this).parent().children('.pseudo-select-list');
    if (list.css('display') === 'none') {
        $('.pseudo-select').removeClass('pseudo-select-clicked');
        $('.pseudo-select-list').hide();
        $(this).parent().addClass('pseudo-select-clicked');
        pseudoselect.closeClick = false;
        pseudoselect.closeSelectOnClicksNumber = 0;
        list.show();
        pseudoselect.closeClick = true;
        pseudoselect.closeSelectOnClicksNumber = pseudoselect.clicks + 2;
    } else {
        $(this).parent().removeClass('pseudo-select-clicked');
        list.hide();
        pseudoselect.closeClick = false;
        pseudoselect.closeSelectOnClicksNumber = 0;
    }
});

$(document).on('click', '.pseudo-select-arrow', function () {
    var list = $(this).parent().children('.pseudo-select-list');
    if (list.css('display') === 'none') {
        $('.pseudo-select').removeClass('pseudo-select-clicked');
        $('.pseudo-select-list').hide();
        $(this).parent().addClass('pseudo-select-clicked');
        pseudoselect.closeClick = false;
        pseudoselect.closeSelectOnClicksNumber = 0;
        list.show();
        pseudoselect.closeClick = true;
        pseudoselect.closeSelectOnClicksNumber = pseudoselect.clicks + 2;
    } else {
        $(this).parent().removeClass('pseudo-select-clicked');
        list.hide();
        pseudoselect.closeClick = false;
        pseudoselect.closeSelectOnClicksNumber = 0;
    }
});

$(document).on('click', '.pseudo-select-list-item', function () {
    $(this).parent().parent().removeClass('pseudo-select-clicked');
    $(this).parent().parent().children('.pseudo-select-selected').html($(this).html());
    $(this).parent().parent().children('.pseudo-select-list').hide();
    $(this).parent().children('.pseudo-select-list-item').removeClass('pseudo-select-list-item-selected');
    $(this).addClass('pseudo-select-list-item-selected');
    $(this).parent().parent().children('.pseudo-select-input').val($(this).attr('name')).trigger('change');
    pseudoselect.closeClick = false;
    pseudoselect.closeSelectOnClicksNumber = 0;
});

$(document).ready(function () {
    initializePseudoSelect();
});
