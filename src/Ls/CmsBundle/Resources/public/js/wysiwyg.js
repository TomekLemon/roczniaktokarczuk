$(function () {
    fckconfig_common = {
        skin: 'BootstrapCK-Skin',
        contentsCss: ['http://fonts.googleapis.com/css?family=Roboto+Slab:400,300,100,700&subset=latin,latin-ext', '/bundles/lscms/css/editor.css'],
        bodyClass: 'editor',
        stylesSet: [
            {name: 'Normalny', element: 'p', attributes: {'class': ''}},
            {name: 'Wyróżnienie', element: 'p', attributes: {'class': 'wyroznienie'}},
            {name: 'Podtytuł', element: 'h3', attributes: {'class': 'podtytul'}}
        ],
        filebrowserBrowseUrl: kcfinderBrowseUrl + '?type=files',
        filebrowserImageBrowseUrl: kcfinderBrowseUrl + '?type=images',
        filebrowserFlashBrowseUrl: kcfinderBrowseUrl + '?type=flash',
        filebrowserUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '/bundles/lscms/kcfinder/upload.php?type=flash',
    };

    fckconfig = jQuery.extend(true, {
        height: '400px',
        width: '700px'
    }, fckconfig_common);

    $('.wysiwyg').ckeditor(fckconfig);
});