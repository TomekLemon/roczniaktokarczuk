<?php

namespace Ls\CmsBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Ls\CmsBundle\Utils\Tools;
use Ls\CmsBundle\Entity\News;

class NewsUpdater implements EventSubscriber {

    public function getSubscribedEvents() {
        return array(
            'prePersist',
            'postPersist',
            'preUpdate',
            'postUpdate',
            'postRemove',
        );
    }

    public function prePersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            if (!$entity->getCreatedAt()) {
                $entity->setCreatedAt(new \DateTime());
            }
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $entity->setSeoKeywords(implode(', ', $keywords));
                $entity->setSeoTitle(strip_tags($entity->getTitle()));
                $entity->setSeoDescription($description);
                $entity->setSeoGenerate(false);
            }
            if ($entity->getContentShortGenerate()) {
                $content_short = strip_tags($entity->getContent());
                $content_short = Tools::truncateWord(html_entity_decode($content_short), 255, '');
                $content_short = trim($content_short);
                $entity->setContentShort($content_short);
                $entity->setContentShortGenerate(false);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof News) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                if (null !== $entity->getOldSlug()) {
                    $query = $em->createQueryBuilder()
                        ->select('c')
                        ->from('LsCmsBundle:MenuItem', 'c')
                        ->where('c.route LIKE :route')
                        ->andWhere('c.routeParameters LIKE :slug')
                        ->setParameter('route', 'lscms_news_show')
                        ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                        ->getQuery();

                    $items = $query->getResult();

                    foreach ($items as $item) {
                        $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                        $em->persist($item);
                    }
                    $em->flush();

                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                } else {
                    $entity->setOldSlug($entity->getSlug());
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function preUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            $entity->setUpdatedAt(new \DateTime());
            if ($entity->getSeoGenerate()) {
                // description
                $description = strip_tags($entity->getContent());
                $description = html_entity_decode($description, ENT_COMPAT | ENT_HTML401, 'UTF-8');
                $description = str_replace("\xC2\xA0", ' ', $description);
                $description = Tools::truncateWord($description, 255, '');

                // usunięcie nowych linii
                $description = preg_replace('@\v@', ' ', $description);
                // podwójnych białych znaków
                $description = preg_replace('@\h{2,}@', ' ', $description);

                // usunięcie ostatniego, niedokończonego zdania
                $description = preg_replace('@(.*)\..*@', '\1.', $description);

                // trim
                $description = trim($description);

                // keywords
                $keywords_arr = explode(' ', $entity->getTitle() . ' ' . $description);

                $keywords = array();
                if (is_array($keywords_arr)) {
                    foreach ($keywords_arr as $kw) {
                        $kw = trim($kw);
                        $kw = preg_replace('/[\.,;\'\"]/', '', $kw);
                        if (strlen($kw) >= 4 && !in_array($kw, $keywords)) {
                            $keywords[] = $kw;
                        }
                        if (count($keywords) >= 10) {
                            break;
                        }
                    }
                }

                $keywords = implode(', ', $keywords);
                $args->setNewValue('seo_title', strip_tags($entity->getTitle()));
                $args->setNewValue('seo_keywords', $keywords);
                $args->setNewValue('seo_description', $description);
                $args->setNewValue('seo_generate', false);
            }
            if ($entity->getContentShortGenerate()) {
                $content_short = strip_tags($entity->getContent());
                $content_short = Tools::truncateWord(html_entity_decode($content_short), 255, '');
                $content_short = trim($content_short);
                $args->setNewValue('content_short', $content_short);
                $args->setNewValue('content_short_generate', false);
            }
        }
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();

        if ($entity instanceof News) {
            if (strcmp($entity->getOldSlug(), $entity->getSlug()) !== 0) {
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('LsCmsBundle:MenuItem', 'c')
                    ->where('c.route LIKE :route')
                    ->andWhere('c.routeParameters LIKE :slug')
                    ->setParameter('route', 'lscms_news_show')
                    ->setParameter('slug', '%' . $entity->getOldSlug() . '%')
                    ->getQuery();

                $items = $query->getResult();

                foreach ($items as $item) {
                    $item->setRouteParameters('{"slug":"' . $entity->getSlug() . '"}');
                    $em->persist($item);
                }
                $em->flush();

                $entity->setOldSlug($entity->getSlug());
                $em->persist($entity);
                $em->flush();
            }
        }
    }

    public function postRemove(LifecycleEventArgs $args) {
        $entity = $args->getEntity();

        if ($entity instanceof News) {
            $entity->deletePhoto();
        }
    }

}