<?php

namespace Ls\CmsBundle\Form;

use Ls\CmsBundle\Validator\Constraints\GoogleRecaptcha;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType {
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array(
                'label' => 'Imię i nazwisko',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Podaj imię i nazwisko'))
                )
            )
        );
        $builder->add('phone', null, array(
                'label' => 'Numer telefonu',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array('message' => 'Podaj numer telefonu'))
                )
            )
        );
        $builder->add('email', null, array(
                'label' => 'Adres e-mail',
                'required' => true,
                'constraints' => array(
                    new NotBlank(array(
                        'message' => 'Podaj adres e-mail'
                    )),
                    new Email(array(
                        'message' => 'Podaj poprawny adres e-mail'
                    ))
                )
            )
        );
        $builder->add('time', 'hidden', array(
                'label' => 'Kiedy możemy zadzwonić',
                'required' => true,
                'error_bubbling' => false,
                'constraints' => array(
                    new GreaterThan(array(
                        'value' => 0,
                        'message' => 'Wybierz godzinę kontaktu'
                    ))
                )
            )
        );
        $builder->add('content', 'textarea', array(
                'label' => 'Treść (opcjonalnie)',
                'required' => false
            )
        );
        $builder->add('google_recaptcha', new GoogleRecaptchaType($this->container), array(
                'label' => 'Ochrona antyspamowa',
                'error_bubbling' => false,
                'constraints' => array(
                    new GoogleRecaptcha()
                )
            )
        );
        $builder->add('g-recaptcha-response', null, array(
                'mapped' => false
            )
        );
        $builder->add('submit', 'submit', array(
                'label' => 'Wyślij'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array());
    }

    /**
     * @return string
     */
    public function getName() {
        return 'form_contact';
    }
}
